import time
import random


class FakeInstrument:
    def __init__(self):
        # here is where the connection to the instrument gets initialized (through visa resourcemanager or something similar)
        # we won't do much here for this fake instrument
        self.inst = {}

    def measure(self, wavelength):
        # lets simulate a measurement
        # we'll wait for a bit, then return a random number
        time.sleep(1)
        return random.randint(0, 100)

    def close(self):
        # do anything needed to clean up
        print("cleaning up fake instrument")

