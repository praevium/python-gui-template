import os
import time
import tkinter as tk
import traceback

from gui_update import GUIUpdate
from command import Command

GRID_H = 4
GRID_W = 4
NORMAL_W = 1920
NORMAL_H = 1200


class GUI:
    def __init__(self, root, gui_queue, cmd_queue, logger):
        self.gui_queue = gui_queue
        self.cmd_queue = cmd_queue
        self.root = root
        self.logger = logger

        # instance variables
        self.some_data = []

        self.setup_main_window()

    def process_messages(self):
        """Handle all messages currently in the queue, if any."""
        i = 0
        try:
            while self.gui_queue.qsize() and i < 10:
                msg = self.gui_queue.get(0)
                self.perform_gui_update(msg)
                i += 1
        except Exception as e:
            self.logger.warning(e, exc_info=True)

    def perform_gui_update(self, update):
        """ receives a messsage and updates gui accordingly"""
        switcher = {
            "status": self.update_status,
            "update_data": self.update_data,
        }
        func = switcher.get(
            update.name, lambda _: print(f"Unknown gui update '{update.name}' payload: {update.payload}")
        )
        func(update.payload)

    def queue_status_update(self, text):
        """ queue a status update """
        self.gui_queue.put(GUIUpdate("status", {"text": text}))

    # command queue handling methods
    def queue_command(self, command):
        """ add a command to the queue """
        self.cmd_queue.put(command)

    def flush_commands(self):
        """ Remove all items from command queue """
        with self.cmd_queue.mutex:
            self.cmd_queue.queue.clear()

    def flush_gui_updates(self):
        """ Remove all items from gui queue """
        with self.gui_queue.mutex:
            self.gui_queue.queue.clear()

    # button event handlers
    def exit_pressed(self):
        self.flush_commands()
        self.flush_gui_updates()
        self.queue_command(Command("exit_program"))

    def stop_pressed(self):
        self.flush_commands()
        self.flush_gui_updates()
        self.queue_status_update("Halted by user")

    def measure_pressed(self):
        cmd = Command("measure_instrument", {"wavelength": 1310})
        self.queue_command(cmd)

    # helpers
    def get_input_value(self, input_ref):
        raw_val = input_ref.get()
        if raw_val:
            return int(raw_val)
        return 0

    def set_input_value(self, input_ref, value):
        input_ref.delete(0, tk.END)
        input_ref.insert(0, value)

    # gui updaters
    def update_status(self, payload):
        """ Update the text of the status label """
        self.status.configure(text=payload["text"])

    def update_data(self, payload):
        """ Update the text of the data entry """
        self.set_input_value(self.instrument_data, payload["data"])

    # gui setup methods
    def setup_main_window(self):
        """ Instance variables declared: font_size """
        self.root.title("My Instrument")
        self.root.geometry("400x200")

        # on window close, act like exit button pressed
        self.root.protocol("WM_DELETE_WINDOW", self.exit_pressed)

        screen_width = self.root.winfo_screenwidth()
        screen_height = self.root.winfo_screenheight()

        percentage_width = screen_width / NORMAL_W
        percentage_height = screen_height / NORMAL_H

        scale_factor = (percentage_width + percentage_height) / 2

        # calculate self.font_size based on scale_factor,
        font_size = int(14 * scale_factor)
        minimum_size = 8
        if font_size < minimum_size:
            font_size = minimum_size

        self.font_size = font_size
        # configure ttk style
        s = tk.ttk.Style()
        s.configure(".", font=("Helvetica", self.font_size))
        s.configure("big.TButton", font=("Helvetica", self.font_size + 2))

        exit_button = tk.ttk.Button(self.root, text="EXIT", style="big.TButton", command=self.exit_pressed)
        exit_button.grid(row=1, column=0, sticky="nesw")

        stop_button = tk.ttk.Button(self.root, text="STOP", style="big.TButton", command=self.stop_pressed)
        stop_button.grid(row=2, column=0, sticky="nesw")

        # Add status bar
        self.status = tk.ttk.Label(self.root, text="Status")
        self.status.grid(row=2, column=1)

        # elements for taking measurement
        instrument_data_label = tk.ttk.Label(self.root, text="Data")
        instrument_data_label.grid(row=5, column=0)
        self.instrument_data = tk.ttk.Entry(self.root)
        self.instrument_data.grid(row=5, column=1)

        self.take_measurement_btn = tk.ttk.Button(self.root, text="Measure", command=self.measure_pressed)
        self.take_measurement_btn.grid(row=5, column=3, sticky="nesw")
