import logging
import sys
import threading
import time
import traceback
from queue import Empty, Queue

from ttkthemes import ThemedTk

from fake_instrument import FakeInstrument
from gui import GUI
from gui_update import GUIUpdate

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class MyInstrument:
    def __init__(self, root):
        self.root = root
        # init queue used to pass messages from worker thread to update GUI
        self.gui_queue = Queue()
        # init queue used to send commands from GUI to worker cmd
        self.cmd_queue = Queue()

        # setup gui
        self.gui = GUI(root, self.gui_queue, self.cmd_queue, logger)

        #  here is where you would want to intialize any instruments
        self.fake_instrument = FakeInstrument()

        # setup worker thread
        self.is_running = True
        self.thread = threading.Thread(target=self.worker_thread)
        self.thread.start()

        # call function that tells gui to check its queue
        self.periodic_call()

    def periodic_call(self):
        """
        Check every periodically if there is something new in the queue.
        """
        self.gui.process_messages()
        if not self.is_running:
            # This is the brutal stop of the system. You may want to do
            # some cleanup before actually shutting it down.

            self.cleanup()
            self.root.destroy()
            sys.exit()
        self.root.after(200, self.periodic_call)

    def cleanup(self):
        self.fake_instrument.close()

    def worker_thread(self):
        """ This is where we handle the asynchronous I/O. """
        while self.is_running:
            if self.cmd_queue.qsize():
                command = self.cmd_queue.get()
                self.perform_command(command)

    def perform_command(self, cmd):
        """ Calls a function based on command passed from gui """
        commands = {
            "exit_program": self.exit_program,
            "measure_instrument": self.measure_instrument,
        }
        # get function reference from dict. Lambda is returned if key not found
        selected_function = commands.get(
            cmd.name, lambda cmd: print(f"Unknown command '{cmd.name}' payload: {cmd.payload}")
        )

        # execute function, passing the command as an argument
        try:
            selected_function(cmd.payload)
        except Exception as e:
            # log all exceptions as a warning instead of raising
            logger.warning(e, exc_info=True)

    def queue_gui_update(self, update):
        """ send message to gui, messgae name is derived from command """
        self.gui_queue.put(update)

    def queue_status_update(self, text):
        """ Update status in gui """
        self.queue_gui_update(GUIUpdate("status", {"text": text}))

    # command functions
    def exit_program(self, payload):
        """Event that tells main thread to clean up and exit"""
        self.queue_status_update("Exiting Program")
        self.is_running = False

    def measure_instrument(self, payload):
        """ get a measurement from our fake instrument """
        wavelength = payload["wavelength"]
        data = self.fake_instrument.measure(wavelength)
        self.queue_status_update("Measurement taken")
        update = GUIUpdate("update_data", {"data": data})
        self.queue_gui_update(update)


def main():
    root = ThemedTk(theme="equilux")
    tester = MyInstrument(root)
    root.mainloop()


# execute main function if this program is run directly
if __name__ == "__main__":
    main()
